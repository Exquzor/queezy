require('dotenv').config(); // Load environment variables

const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const cors = require('cors');
const jwt = require('jsonwebtoken');

// Import routes
const authRoutes = require('./authRoutes');
const profileRoutes = require('./profileRoutes');
const quizRoutes = require('./quizRoutes');

const app = express();

app.use(cookieParser());

app.use(bodyParser.json());

app.use(cors({
    origin: 'http://localhost:3000',
    credentials: true,
}));

app.use('/auth', authRoutes);

app.use('/user', profileRoutes);

app.use('/quiz', quizRoutes);

// Server setup
const PORT = process.env.PORT || 5000;
app.listen(PORT, () => {
    console.log(`Server running on port ${PORT}`);
});
