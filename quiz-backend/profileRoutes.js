const express = require('express');
const db = require('./db');
const router = express.Router();

// Route for user profile
router.get('/profile', async (req, res) => {
    try {
        const userId = req.cookies && req.cookies.userId;

        if (!userId) {
            console.error('Unauthorized: User ID not found. Cookies:', req.cookies);
            return res.status(401).json({ message: 'Unauthorized: User ID not found' });
        }

        // Fetch user details from the database
        const [userResult] = await db.query('SELECT username, game_counter FROM users WHERE id = ?', [userId]);

        if (!userResult.length) {
            console.error('User not found for ID:', userId);
            return res.status(404).json({ message: 'User not found' });
        }

        const user = userResult[0];

        res.json({ username: user.username, game_counter: user.game_counter });
    } catch (error) {
        console.error('Error fetching user profile:', error);
        res.status(500).json({ message: 'Internal server error' });
    }
});

module.exports = router;