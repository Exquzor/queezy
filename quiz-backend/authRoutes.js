const express = require('express');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const db = require('./db');

const router = express.Router();

// Route for user registration
router.post('/register', async (req, res) => {
    try {
      const { username, password } = req.body;
  
      const hashedPassword = await bcrypt.hash(password, 10);
  
      // Save user details to the database
      const result = await db.query('INSERT INTO users (username, password) VALUES (?, ?)', [username, hashedPassword]);
  
      res.status(201).json({ message: 'User registered successfully' });
    } catch (error) {
      console.error(error);
      res.status(500).json({ message: 'Internal server error' });
    }
  });

// Route for user login
router.post('/login', async (req, res) => {
  try {
    const { username, password } = req.body;

    // Retrieve user details from the database
    const [userResult] = await db.query('SELECT * FROM users WHERE username = ?', [username]);

    // Destructuring the result
    const [user] = userResult;

    // Authenticating the credentials
    if (!user || !(await bcrypt.compare(password, user.password))) {
      return res.status(401).json({ message: 'Invalid username or password' });
    }

    // Generate a JWT token for authentication
    const token = jwt.sign({ userId: user.id }, process.env.JWT_SECRET, { expiresIn: '1d' });

    res.json({ token, userId: user.id });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Internal server error' });
  }
});

module.exports = router;
