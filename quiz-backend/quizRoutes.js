const express = require('express');
const db = require('./db');

const router = express.Router();

// Route for getting random questions for a category
router.get('/getRandomQuestions', async (req, res) => {
    try {
        const category = req.query.category;
        const count = parseInt(req.query.count) || 5;

        // Fetch random questions from the database for the specified category
        const query = 'SELECT * FROM questions WHERE category = ? ORDER BY RAND() LIMIT ?';
        const [questions] = await db.query(query, [category, count]);

        if (!questions.length) {
            return res.status(404).json({ message: 'No questions found for the specified category' });
        }

        // Fetch answers for the selected questions
        const questionIds = questions.map(question => question.question_id);
        const answersQuery = 'SELECT * FROM answers WHERE question_id IN (?)';
        const [answers] = await db.query(answersQuery, [questionIds]);

        // Map answers to their respective questions
        const questionsWithAnswers = questions.map(question => {
            question.answers = answers.filter(answer => answer.question_id === question.question_id);
            return question;
        });

        res.json(questionsWithAnswers);
    } catch (error) {
        console.error('Error fetching random questions:', error);
        res.status(500).json({ message: 'Internal server error' });
    }
});

// Route for sumbitting the quiz
router.post('/submitQuiz', async (req, res) => {

    const userId = req.body.userId;

    updateUserQuizCounter(userId);

    res.json({ success: true });
});

// Update the user's quiz counter in the database
function updateUserQuizCounter(userId) {

    const query = `
        UPDATE users
        SET game_counter = game_counter + 1
        WHERE id = ?;
    `;

    db.query(query, [userId], (err, results) => {
        if (err) {
            console.error('Error updating quiz counter:', err);
        }
    });
}

module.exports = router;
