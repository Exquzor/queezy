-- Database setup file

CREATE DATABASE IF NOT EXISTS quizdb;
USE quizdb;

CREATE TABLE users (
    id INT AUTO_INCREMENT PRIMARY KEY,
    username VARCHAR(255) NOT NULL UNIQUE,
    password VARCHAR(255) NOT NULL,
    game_counter INT DEFAULT 0
);

CREATE TABLE questions (
    question_id INT AUTO_INCREMENT PRIMARY KEY,
    category VARCHAR(50),
    question_text TEXT
);

CREATE TABLE answers (
    answer_id INT AUTO_INCREMENT PRIMARY KEY,
    question_id INT,
    answer_text TEXT,
    is_correct BOOLEAN,
    FOREIGN KEY (question_id) REFERENCES questions(question_id)
);