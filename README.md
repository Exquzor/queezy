<a name="introduction"></a>
# Queezy

A web application in a form of an internet quiz game created using JavaScript.

<a name="table-of-contents"></a>
## Table of contents

1. [Introduction](#introduction)
2. [Table of contents](#table-of-contents)
3. [Description](#description)
4. [Author](#author)
5. [Repository](#repository)
6. [Used resources](#used-resources)
7. [How to run](#how-to-run)

<a name="description"></a>
## Description

The goal of this project was to develop a web application in a form of an internet quiz game. The final project allows users to play variety of quizzes on the website in many different categories.
List of categories that implemented:
- English;
- Geography;
- History;
- Math;
- Popculture (eg. Movies, TV Shows, Books, Games, Music);
- Science (eg. Biology, Chemistry, Physics, Astronomy);
- Sports;

The project was carried out as part of the 'Internet Programming' course in the fifth semester (2023/2024) of the non-stationary computer science program at Gdańsk University of Technology.

<a name="author"></a>
## Author

- Name: Łukasz Kluskiewicz
- E-mail: lukkluskiewicz@gmail.com

<a name="repository"></a>
## Repository

The **[GitLab](https://gitlab.com/Exquzor/queezy/)** repository is used for displaying the project and potential future development and version control.

<a name="used-resources"></a>
## Used resources

- Programming languages: HTML (with CSS), JavaScript, SQL;
- **[React.js](https://react.dev/)** - frontend JavaScript library for building user interfaces;
- **[Express.js](https://expressjs.com/)** - backend web application framework for building servers and APIs;
- **[Node.js](https://nodejs.org/en/)** - JavaScript runtime environment;
- **[MySQL](https://www.mysql.com/)** - relational database management system;
- **[GitLab](https://about.gitlab.com/)** - source code repository;
- **[Visual Studio](https://visualstudio.microsoft.com/pl/vs/)** - IDE;
- **[Postman](https://www.postman.com/)** - platform for building and testing APIs;

<a name="how-to-run"></a>
## How to run

The whole system is run locally on user's device.
- Firstly, user has to setup a local MySQL database. The file 'setup.sql' in 'quiz-database' directory showcases the structure of the database;
- Secondly, in the quiz-backend directory user needs to exectue 'node server.js' command to start the server;
- Lastly, in the quiz-frontend directory user needs to execute 'npm start' command to start the react app;