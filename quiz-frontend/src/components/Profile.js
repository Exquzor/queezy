import React, { useState, useEffect } from 'react';
import { useNavigate, Link } from 'react-router-dom';
import Navbar from './Navbar';
import axios from 'axios';
import './Profile.css';

const Profile = () => {
    const [profile, setProfile] = useState(null);
    const navigate = useNavigate();

    useEffect(() => {
        const userId = getCookie('userId');

        if (!userId) {
            console.error('User ID not found');
            navigate('/'); // Redirect to home page when the cookie is not found
            return;
        }

        // Fetch user profile data
        axios.get('http://localhost:5000/user/profile', {
            withCredentials: true,
        })
            .then(response => {
                if (response.status === 401) {
                    console.log('User not authenticated');
                    navigate('/'); // Redirect to home page when the userId is wrong
                    return null;
                }
                return response.data;
            })
            .then(data => {
                if (data) {
                    setProfile(data);
                }
            })
            .catch(error => {
                console.error('Error fetching profile:', error);
            });
    }, [navigate]);

    const getCookie = (name) => {
        const value = `; ${document.cookie}`;
        const parts = value.split(`; ${name}=`);
        if (parts.length === 2) return parts.pop().split(';').shift();
    };

    return (
        <div>
            <Navbar />
            <div className="profile-logo-container">
                <img src="queezy-logo.png" alt="Queezy Logo" />
            </div>
            <div className="profile-container">
                <h1>User Profile</h1>
                {profile ? (
                    <div>
                        <p>Username: {profile.username}</p>
                        <p>Queezies played: {profile.game_counter}</p>
                    </div>
                ) : (
                    <p>Loading...</p>
                )}
            </div>
        </div>
    );
};

export default Profile;