import React, { useState } from 'react';
import axios from 'axios';
import { useCookies } from 'react-cookie';
import Navbar from './Navbar';
import './Login.css';
import { Link } from 'react-router-dom';

const Login = () => {
    
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [cookies, setCookie] = useCookies(['authToken']);

    
    const handleLogin = async (e) => {
        e.preventDefault();

        try {
            const response = await axios.post('http://localhost:5000/auth/login', { username, password });

            const { token, userId } = response.data;

            setCookie('authToken', token, { path: '/' });
            setCookie('userId', userId, { path: '/' });

            console.log('Login successful');

        } catch (error) {
            
            console.error('Login error:', error);
        }
    };

    return (
        <div>
            <Navbar />
            <div className="login-container">
                <Link to="/">
                    <img src="queezy-logo.png" alt="Queezy Logo" />
                </Link>
                <h2>Login</h2>
                <form onSubmit={handleLogin}>
                    <div className="login-input-group">
                        <label>Username:</label>
                        <input type="text" value={username} onChange={(e) => setUsername(e.target.value)} />
                    </div>
                    <div className="login-input-group">
                        <label>Password:</label>
                        <input type="password" value={password} onChange={(e) => setPassword(e.target.value)} />
                    </div>
                    <button type="submit">Login</button>
                </form>
            </div>
        </div>
    );
};

export default Login;
