import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { useCookies } from 'react-cookie';
import './Navbar.css';

const Navbar = () => {

    const [isLoggedIn, setIsLoggedIn] = useState(false);
    const [cookies] = useCookies(['authToken', 'userId']);

    useEffect(() => {
        // Check if the authentication token exists in cookies
        const authToken = cookies.authToken;
        setIsLoggedIn(!!authToken);
    }, [cookies]);

    // Remove the cookies upon logging out
    const handleLogout = () => {
        document.cookie = 'authToken=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;';
        document.cookie = 'userId=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;';
        setIsLoggedIn(false);
    };

    return (
        <div className="navbar">
            <nav>
                <Link to="/">
                    <img src="queezy-logo.png" alt="Queezy Logo" />
                </Link>

                <Link to="/">Home</Link>

                {isLoggedIn ? (
                    <>
                        <Link to="/profile">Profile</Link>
                        <button onClick={handleLogout}>Logout</button>
                    </>
                ) : (
                    <>
                        <Link to="/register">Register</Link>
                        <Link to="/login">Login</Link>
                    </>
                )}
            </nav>
        </div>
    );
};

export default Navbar;
