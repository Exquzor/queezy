import React from 'react';
import './Home.css';
import Navbar from './Navbar';
import { Link } from 'react-router-dom';

const Home = () => {
    return (
        <div>
            <Navbar />
            <div className="home-container">
                <Link to="/">
                    <img src="queezy-logo.png" alt="Queezy Logo" />
                </Link>
                <h1>Welcome to Queezy!</h1>
                <h2>What is Queezy?<br />
                    It's simple!<br />
                    A web app that allows you to test your knowledge by playing quizzes from many various categories.<br />
                    If you want to start the quiz click the button below and enjoy!</h2>

                <div className="home-buttons">
                    <Link to="/singleplayer">
                        <button>Play Queezy</button>
                    </Link>
                </div>
            </div>
        </div>
    );
}

export default Home;
