import { useCookies } from 'react-cookie';

const useCurrentUserId = () => {
    const [cookies] = useCookies(['userId']);
    return cookies.userId || null;
};

export default useCurrentUserId;
