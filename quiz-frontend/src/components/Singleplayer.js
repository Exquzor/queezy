import React, { useState } from 'react';
import Navbar from './Navbar';
import './Singleplayer.css';
import useCurrentUserId from './useCurrentUserId';

const Singleplayer = () => {
    const [questions, setQuestions] = useState([]);
    const [currentQuestion, setCurrentQuestion] = useState(0);
    const [score, setScore] = useState(0);
    const [answerFeedback, setAnswerFeedback] = useState(null);
    const [quizEnded, setQuizEnded] = useState(false);
    const [selectedCategory, setSelectedCategory] = useState(null);
    const [categorySelected, setCategorySelected] = useState(false);

    const categories = [
        {
            name: 'English',
            image: 'english-icon.png',
        },
        {
            name: 'Geography',
            image: 'geography-icon.png',
        },
        {
            name: 'History',
            image: 'history-icon.png',
        },
        {
            name: 'Math',
            image: 'math-icon.png',
        },
        {
            name: 'Popculture',
            image: 'popculture-icon.png',
        },
        {
            name: 'Science',
            image: 'science-icon.png',
        },
        {
            name: 'Sports',
            image: 'sports-icon.png',
        },
    ];

    const handleCategoryClick = (category) => {
        fetch(`http://localhost:5000/quiz/getRandomQuestions?category=${category}&count=5`)
            .then(response => response.json())
            .then(data => {

                // Extract questions and answers from the response
                const formattedQuestions = data.map(question => {
                    // Shuffle the answers array
                    const shuffledAnswers = shuffleArray(question.answers);

                    const formattedAnswers = shuffledAnswers.map(answer => {
                        return {
                            answer_text: answer.answer_text,
                            is_correct: answer.is_correct === 1,
                        };
                    });

                    return {
                        question_text: question.question_text,
                        answers: formattedAnswers,
                        correctAnswer: formattedAnswers.find(answer => answer.is_correct).answer_text,
                    };
                });

                setQuestions(formattedQuestions);
                setCurrentQuestion(0);
                setScore(0);
                setAnswerFeedback(null);
                setQuizEnded(false);
                setCategorySelected(true);
                setSelectedCategory(category);
            })
            .catch(error => {
                console.error('Error fetching random questions:', error);
            });
    };

    // Function to shuffle an array
    const shuffleArray = (array) => {
        for (let i = array.length - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            [array[i], array[j]] = [array[j], array[i]];
        }
        return array;
    };


    const handleReplayClick = () => {
        setCategorySelected(false);
    };

    const userId = useCurrentUserId();

    const handleAnswerClick = (selectedAnswer) => {
        const currentQuestionData = questions[currentQuestion];

        if (selectedAnswer.answer_text === currentQuestionData.correctAnswer) {
            setScore((prevScore) => prevScore + 1);
            setAnswerFeedback('Correct answer!');
        } else {
            setAnswerFeedback('Wrong answer');
        }

        // Automatically move to the next question after 1 second
        setTimeout(() => {
            setAnswerFeedback(null);
            if (currentQuestion < questions.length - 1) {
                setCurrentQuestion((prevQuestion) => prevQuestion + 1);
            } else {
                // End of the quiz
                setQuizEnded(true);

                submitQuizResults();
            }
        }, 1000);
    };


    const submitQuizResults = () => {
        if (!userId) {
            console.error('User ID not found');
            return;
        }

        // Make a request to the server to submit the quiz results
        fetch('http://localhost:5000/quiz/submitQuiz', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ userId }),
        })
            .then(response => response.json())
            .then(data => {
                console.log('Quiz results submitted successfully:', data);
            })
            .catch(error => {
                console.error('Error submitting quiz results:', error);
            });
    };


    return (
        <div>
            <Navbar />
            {!categorySelected && (
                <div className="category-container">
                    <h1>Choose a Queezy Category</h1>
                    <ul>
                        {categories.map((category, index) => (
                            <li key={index} onClick={() => handleCategoryClick(category.name)}>
                                <img
                                    src={`${process.env.PUBLIC_URL}/${category.image}`}
                                    alt={category.name}
                                />
                                <span>{category.name}</span>
                            </li>
                        ))}
                    </ul>
                </div>
            )}
            {categorySelected && !quizEnded && (
                <div className="quiz-container">
                    <h2>{selectedCategory} Quiz</h2>
                    <h3>Question {currentQuestion + 1}</h3>
                    <p>{questions[currentQuestion].question_text}</p>
                    <ul>
                        {questions[currentQuestion].answers.map((answer, index) => (
                            <li key={index} onClick={() => handleAnswerClick(answer)}>
                                {answer.answer_text}
                            </li>
                        ))}
                    </ul>
                    {answerFeedback && (
                        <p className={`feedback ${answerFeedback === 'Correct answer!' ? 'correct' : 'wrong'}`}>
                            {answerFeedback}
                        </p>
                    )}
                </div>
            )}
            {quizEnded && categorySelected && (
                <div className="summary">
                    <h1>Quiz Summary</h1>
                    <p>Your score: {score} out of {questions.length}</p>
                    <button onClick={handleReplayClick}>Replay Queezy</button>
                </div>
            )}
        </div>
    );
};

export default Singleplayer;