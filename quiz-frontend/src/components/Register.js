import React, { useState } from 'react';
import Navbar from './Navbar';
import './Register.css';
import { Link } from 'react-router-dom';

const Register = () => {

    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
  
    // Function to handle form submission
    const handleRegister = async (e) => {
      e.preventDefault();
  
      // Make a POST request to backend registration route with the user's details
      try {
        const response = await fetch('http://localhost:5000/auth/register', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({ username, password }),
        });
  
        const data = await response.json();

      } catch (error) {
        console.error('Registration failed', error);
      }
    };

    return (
    <div>
        <Navbar />
        <div className="register-container">
            <Link to="/">
                <img src="queezy-logo.png" alt="Queezy Logo" />
            </Link>
            <h2>Registration</h2>
            <form onSubmit={handleRegister}>
                <div className="register-input-group">
                    <label>Username:</label>
                    <input type="text" value={username} onChange={(e) => setUsername(e.target.value)} />
                </div>
            <div className="register-input-group">
                <label>Password:</label>
                <input type="password" value={password} onChange={(e) => setPassword(e.target.value)} />
            </div>
            <button type="submit">Register</button>
            </form>
        </div>
    </div>
  );
};

export default Register;
