import './App.css';
import React from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Register from './components/Register';
import Login from './components/Login';
import Home from './components/Home';
import Singleplayer from './components/Singleplayer';
import Profile from './components/Profile';



function App() {
    return (
        <Router>
            <Routes>
                <Route path='/' exact element={<Home />} />
                <Route path='/register' exact element={<Register />} />
                <Route path='/login' exact element={<Login />} />
                <Route path='/singleplayer' exact element={<Singleplayer />} />
                <Route path='/profile' exact element={<Profile />} />
            </Routes>
        </Router>
    );
}

export default App;
